# ICE - an MVC web framework for [Rust](https://www.rust-lang.org/)

ICE (pronounce "ice", the frozen form of water) is an MVC framework for Rust. It's built with [Iron](http://ironframework.io/), pumps your data using [Diesel](http://diesel.rs/), and crystallizes your [Liquid](https://github.com/cobalt-org/liquid-rust) templates to HTML.

## Quick start
Head over to the [simple example](https://gitlab.com/rubdos/ice-mvc/tree/master/examples/simple).

## Implemented components
ICE is everything but complete. Currently, it's not even an MVC framework.

**DONE**:
- Wrapper around the router system for Controller classes.
- Views

**TODO:**
- Add optional session stuff to controller methods.
- Add examples for models using Diesel.
- Template/layout views
- Subviews
- Create a `rails` like executable.
- Long term: support more template systems, support more ORM systems
