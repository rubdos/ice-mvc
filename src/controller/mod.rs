use std::collections::HashMap;
use router::Router;
use liquid::{self, Renderable};
use iron;
use iron::prelude::*;
use urlencoded::{UrlEncodedQuery, UrlDecodingError};
use params::{self, Params, Value};

use serde;
#[macro_use]
use serde_json;

#[macro_export]
macro_rules! ice_method_lambda {
    {
        $layout:ident, $controller:ident, $request_type:tt $path:tt => $method:ident
    } => {{
        |req: &mut ::iron::Request| {
            let view = ::liquid::parse(views::$method,
                                       Default::default()).unwrap();
            let layout = ::liquid::parse(::ice_layouts::$layout,
                                         Default::default()).unwrap();

            info!("{} controller method {}::{} called",
                  stringify!($request_type),
                  stringify!($controller),
                  stringify!($method));

            $controller::call_route(Box::new($controller::$method),
                                    req,
                                    view,
                                    layout)
        }
    }}
}

#[macro_export]
macro_rules! generate_method {
    {
        $router:ident, $layout:ident, $controller:ident, $path:tt => $method:ident
    } => {
        generate_method!($router, $layout, $controller, GET $path => $method);
    };

    {
        $router:ident, $layout:ident, $controller:ident, POST $path:tt => $method:ident
    } => {{
        use iron;
        $router.post($path, ice_method_lambda!($layout, $controller, POST $path => $method), $path);
    }};

    {
        $router:ident, $layout:ident, $controller:ident, GET $path:tt => $method:ident
    } => {{
        use iron;
        $router.get($path, ice_method_lambda!($layout, $controller, GET $path => $method), $path);
    }}
}

#[macro_export]
macro_rules! controller {
    (
        name: $controller:ident, // Controller name
        views: $viewbase:tt,
        methods: {
            $(
                $path:tt => $method:ident $( ( $modifier:ident ) )*
            ),* // Controller methods
            $(,)*
        },
        layout: $layout:ident
    ) => (
        mod views {
            $(
                #[allow(non_upper_case_globals)]
                pub static $method: &'static str = include_str!(
                    concat!($viewbase, "/", stringify!($method), ".liquid"));
            )*
        }

        pub struct $controller {
            view: ::liquid::Template,
            context: ::liquid::Context,
            json_context: ::std::collections::HashMap<String, ::serde_json::Value>,
            query: ::params::Map,
            response: Option<::iron::Response>,
        }
        impl $crate::Routable for $controller {
            fn add_routes(router: &mut ::router::Router) {
                $( generate_method!(router, $layout, $controller, $($modifier)* $path => $method ) );*
            }
            fn new(view: ::liquid::Template,
                   query_map: ::params::Map) -> $controller {
                $controller {
                    view: view,
                    context: ::liquid::Context::new(),
                    json_context: Default::default(),
                    query: query_map,
                    response: None,
                }
            }
            fn render(&mut self) -> String {
                use ::liquid::Renderable;
                self.view.render(&mut self.context).
                    unwrap().unwrap()
            }
            fn render_json(&mut self) -> String {
                ::serde_json::to_string(&self.json_context).unwrap()
            }
            fn get_response(&mut self) -> Option<iron::Response> {
                use std;
                let mut r = None;
                std::mem::swap(&mut r, &mut self.response);
                r
            }
        }

        impl<T> $crate::ControllerContextSetter<T> for $controller
                where T: $crate::view::IntoLiquid + ::serde_json::value::ToJson {
            fn set(&mut self, key: &str, value: T) {
                use $crate::view::IntoLiquid;
                use ::serde_json::value::ToJson;
                self.json_context.insert(key.to_owned(), value.to_json().unwrap());
                self.context.set_val(key, $crate::view::IntoLiquid::into_liquid(value));
            }
        }
    )
}

pub trait ControllerContextSetter<T>
        where T: ?Sized {
    fn set(&mut self, key: &str, value: T);
}

pub trait Routable
        where Self: Sized {
    fn add_routes(&mut Router);
    fn new(liquid::Template, ::params::Map) -> Self;
    fn render(&mut self) -> String;
    fn render_json(&mut self) -> String;
    fn get_response(&mut self) -> Option<iron::Response>;

    fn call_route(method: Box<Fn(&mut Self, &mut iron::Request)>,
                  req: &mut iron::Request,
                  view: liquid::Template,
                  layout: liquid::Template)
            -> iron::IronResult<iron::Response> {
        let map = req.get::<Params>().unwrap();

        let mut controller = Self::new(view, map);

        method(&mut controller, req);

        if let Some(response) = controller.get_response() {
            return Ok(response);
        }

        let is_json_request = if let Some(h) = req.headers.get::<iron::headers::Accept>() {
            debug!("Accept header: {}", h);
            h.iter().any(|x| {
                debug!("{}", x.item);
                match x.item {
                    Mime(TopLevel::Application, SubLevel::Json, _) => true,
                    _ => false,
                }
            })
        } else {
            false
        };


        use iron::headers::ContentType;
        use iron::mime::{Mime, TopLevel, SubLevel};
        if is_json_request {
            info!("Json request");
            let content = controller.render_json();
            let mut response = iron::Response::with((iron::status::Ok, content));
            response.headers.set(ContentType(Mime(TopLevel::Text, SubLevel::Json, vec![])));
            Ok(response)
        } else {
            info!("HTML request");
            let content = controller.render();
            let mut layout_context = liquid::Context::new();
            layout_context.set_val("yield",
                                   liquid::Value::Str(content));
            let content = layout.render(&mut layout_context).
                unwrap().unwrap();
            let mut response = iron::Response::with((iron::status::Ok, content));
            response.headers.set(ContentType(Mime(TopLevel::Text, SubLevel::Html, vec![])));
            Ok(response)
        }
    }
}
