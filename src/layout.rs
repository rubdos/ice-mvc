#[macro_export]
macro_rules! ice_declare_layouts {
    (
        $(
            $name:ident
        ),+
    ) => (
        mod ice_layouts {
            $(
                #[allow(non_upper_case_globals)]
                pub static $name: &'static str = include_str!(
                    concat!("app/layouts/",
                            stringify!($name),
                            ".liquid"));
            )*
        }
    )
}
