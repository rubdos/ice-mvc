use liquid;
use serde::{self, Serialize};
use serde_json;
use serde_json::value::ToJson;

pub trait IntoLiquid {
    fn into_liquid(self) -> liquid::Value;
}

pub trait AsLiquid {
    fn as_liquid(&self) -> liquid::Value;
}

// Implementation for integers

macro_rules! int_to_liquid {
    ($inttype:tt) => {
        impl IntoLiquid for $inttype {
            fn into_liquid(self) -> liquid::Value {
                liquid::Value::Num(Into::into(self))
            }
        }
    }
}

impl IntoLiquid for i32 {
    fn into_liquid(self) -> liquid::Value {
        liquid::Value::Num(Into::into(self as i16))
    }
}

//int_to_liquid!(i32);
int_to_liquid!(u16);
int_to_liquid!(i16);
int_to_liquid!(u8);
int_to_liquid!(i8);
int_to_liquid!(f32);

// Implementation for bool

impl IntoLiquid for bool {
    fn into_liquid(self) -> liquid::Value {
        liquid::Value::Bool(self)
    }
}

// Implementation for string

impl<'a> IntoLiquid for &'a str {
    fn into_liquid(self) -> liquid::Value {
        liquid::Value::Str(String::from(self))
    }
}

impl IntoLiquid for String {
    fn into_liquid(self) -> liquid::Value {
        liquid::Value::Str(self)
    }
}

// Implementation for Vec<S> where S: IntoLiquid

impl AsLiquid for Vec<liquid::Value> {
    fn as_liquid(&self) -> liquid::Value {
        liquid::Value::Array(self.iter().cloned().collect())
    }
}

impl IntoLiquid for Vec<liquid::Value> {
    fn into_liquid(self) -> liquid::Value {
        liquid::Value::Array(self)
    }
}

impl<S: AsLiquid> AsLiquid for Vec<S> {
    fn as_liquid(&self) -> liquid::Value {
        liquid::Value::Array(self.iter().map(|x| AsLiquid::as_liquid(x)).collect())
    }
}

impl<S: IntoLiquid> IntoLiquid for Vec<S> {
    fn into_liquid(self) -> liquid::Value {
        liquid::Value::Array(self.into_iter().map(|x| IntoLiquid::into_liquid(x)).collect())
    }
}

// Implementation for Option<S>

impl<S: IntoLiquid> IntoLiquid for Option<S> {
    fn into_liquid(self) -> liquid::Value {
        match self {
            Some(v) => {
                IntoLiquid::into_liquid(v)
            },
            None => {
                liquid::Value::Bool(false)
            }
        }
    }
}

// Implementation for Diesel models

#[macro_export]
macro_rules! liquid_model {
    (
        name: $x:ident
        fields: {
            $($field:ident),+
        }
    ) => {liquid_model!{
        name: $x
        fields: {
            $($field),+
        }
        resource: $x
    }};
    (
        name: $x:ident
        fields: {
            $($field:ident),+
        }
        resource: $resource:ident
    ) => {liquid_model!{
        name: $x
        fields: {
            $($field),+
        }
        resource: $resource
        calculated_fields: {
        }
    }};
    (
        name: $x:ident
        fields: {
            $($field:ident),+
        }
        resource: $resource:ident
        calculated_fields: {
            $($extra_field:ident),*
        }
    ) => {
        impl<'a> Into<::std::collections::HashMap<String, ::liquid::Value>> for &'a $x {
            fn into(self) -> std::collections::HashMap<String, ::liquid::Value> {
                use ::std::collections::HashMap;
                use ::diesel::associations::Identifiable;

                let id = *self.id(); //[$x::table().primary_key()];
                debug!("Creating HashMap from {}: {}", stringify!($x), id);
                [
                    ("url".to_string(), IntoLiquid::into_liquid(
                            format!(concat!("/",
                                    stringify!($resource),
                                    "/{}"), id))),
                    $(
                        (String::from(stringify!($extra_field)), $crate::view::IntoLiquid::into_liquid(self.$extra_field())),
                    )*
                    $(
                        (String::from(stringify!($field)), $crate::view::IntoLiquid::into_liquid(self.$field.clone())),
                    )+
                ].iter().cloned().collect::<HashMap<_, _>>()
            }
        }

        impl $crate::view::AsLiquid for $x {
            fn as_liquid(&self) -> liquid::Value {
                use liquid::Value;
                Value::Object(Into::<::std::collections::HashMap<_, _>>::into(self))
            }
        }

        impl $crate::view::IntoLiquid for $x {
            fn into_liquid(self) -> liquid::Value {
                $crate::view::AsLiquid::as_liquid(&self)
            }
        }
    }
}

use std::collections::HashMap;

impl IntoLiquid for HashMap<String, liquid::Value> {
    fn into_liquid(self) -> liquid::Value {
        use liquid::Value;
        Value::Object(self)
    }
}

pub struct SquashedModel {
    pub base_model: HashMap<String, ::liquid::Value>,
    pub squashed_models: Vec<(&'static str, ::liquid::Value)>,
    pub json: String,
}

impl Serialize for SquashedModel {
    fn serialize<S: serde::Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
        s.serialize_str(&self.json)
    }
}

impl IntoLiquid for SquashedModel {
    fn into_liquid(self) -> ::liquid::Value {
        let mut m1 = self.base_model;
        for (tag, to_squash) in self.squashed_models {
            m1.insert(String::from(tag), to_squash);
        }
        IntoLiquid::into_liquid(m1)
    }
}

#[macro_export]
macro_rules! squash_model {
    {
        $base_model:tt,
        {
            $($squashed_model:tt),+
        }
    } => {{
        use serde_json::value::ToJson;
        let json = $base_model.to_json();
        $crate::view::SquashedModel {
            base_model: Into::into(&$base_model),
            squashed_models: vec![
                $( (stringify!($squashed_model), $crate::view::IntoLiquid::into_liquid($squashed_model)) ),+
            ],
            json: ::serde_json::to_string(&json.unwrap()).unwrap(),
        }
    }}
}
