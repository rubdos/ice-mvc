extern crate iron;
extern crate router;
extern crate urlencoded;
extern crate params;
extern crate liquid;
extern crate diesel;
#[macro_use]
extern crate log;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

mod controller;
pub use controller::{Routable, ControllerContextSetter};

mod layout;
pub mod view;
