#[macro_use]
extern crate ice_mvc;
extern crate iron;
extern crate liquid;
#[macro_use]
extern crate log;
extern crate router;
extern crate diesel;

mod app;

use iron::prelude::*;

ice_declare_layouts!( default /*, test, other_layout*/);

fn main() {
    let routes = app::routes();
    Iron::new(routes).http("localhost:3000").unwrap();
}
