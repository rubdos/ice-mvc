use ice_mvc::View;

pub enum Welcome {
    Index,
}

impl View for Welcome {
    fn map(&self) -> &'static str {
        match *self {
            Welcome::Index => include_str!("welcome/index.liquid"),
        }
    }
}
