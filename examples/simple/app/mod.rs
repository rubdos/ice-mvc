use router::Router;
use ice_mvc::Routable;

mod controllers;

pub fn routes() -> Router {
    let mut router = Router::new();

    controllers::welcome::WelcomeController::add_routes(&mut router);

    router
}

