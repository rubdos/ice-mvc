use iron;

use ice_mvc::*;

controller!(
    name: WelcomeController,
    views: "welcome",
    methods: {
        "/" => index
    },
    layout: default
);

impl WelcomeController {
    fn index(&mut self, _: &mut iron::Request) {
        self.set("message", "Hello world from Rust!");
        // Currently, only {i,u}{16,8} and f32 are supported as numeric type.
        self.set("num", 9 as u16);
        self.set("float", 9.5 as f32);
        self.set("controller_file", file!());
    }
}
